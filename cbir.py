# -*- coding: utf-8 -*-
"""
@author: owlfonso

"""
import cv2
import numpy as np
import os
from tqdm import trange
from tqdm import tqdm

def get_index_of58(num):
    arr = np.asarray([0,   1,   2,   3,   4,   6,   7,   8,   12,  14,  15,  
                      16,  24,  28,  30,  31,  32,  48,  56,  60,  62,  63,  
                      64,  96,  112, 120, 124, 126, 127, 128, 129, 131, 135, 
                      143, 159, 191, 192, 193, 195, 199, 207, 223, 224, 225, 
                      227, 231, 239, 240, 241, 243, 247, 248, 249, 251, 252, 
                      253, 254, 255
            ])
    i = 0
    for i in range(58):
        if(num == arr[i]):
            return i
    return 58

def get_thresholded(current, pixels):
    arr = []
    for px in pixels:
        if px >= current:
            arr.append(1)
        else:
            arr.append(0)
            
    return arr


def get_histogram(arr):
    i = 0
    hist = [0] * 256
    for i in range(len(arr)):
        hist[arr[i]] += 1
    
    return hist

def calc_manhattan_dist(arr1, arr2):
    dist = 0
    for i in range(len(arr1)):
        dist += abs(arr1[i] - arr2[i])
        
    return dist


def create_LBP_histogram(img):
    pixel_count = len(img) * len(img[0])
    results = []
    print("3x3 LBP matrix computations..")
    for x in range(1, len(img)-1):
        for y in range(1, len(img[0])-1):
            top_left     = img[x - 1, y - 1]
            top          = img[x, y - 1]
            top_right    = img[x + 1, y - 1]
            left         = img[x - 1, y]
            center       = img[x, y]
            right        = img[x + 1, y]
            bottom_left  = img[x - 1, y + 1]
            bottom       = img[x, y + 1]
            bottom_right = img[x + 1, y + 1]
             
            values = get_thresholded(center, [top_left, top, top_right, right, 
                                              bottom_right, bottom, bottom_left, 
                                              left])

        
            weights = [1, 2, 4, 8, 16, 32, 64, 128]
             
            res = 0
            for ind in range(0, len(values)):
                res += weights[ind] * values[ind]
            
            results.append(res)
            #transformed_img.itemset((x, y), res)
        #endfor
    #endfor
    
    #create non-normalized histogram
    #histogram_LBP = get_histogram(results)
    
    arr = np.asarray([0,   1,   2,   3,   4,   6,   7,   8,   12,  14,  15,  
                      16,  24,  28,  30,  31,  32,  48,  56,  60,  62,  63,  
                      64,  96,  112, 120, 124, 126, 127, 128, 129, 131, 135, 
                      143, 159, 191, 192, 193, 195, 199, 207, 223, 224, 225, 
                      227, 231, 239, 240, 241, 243, 247, 248, 249, 251, 252, 
                      253, 254, 255
                ])
    
    hist = np.zeros((59,), dtype='float16')
    print("Creating LBP histogram..")
    for r in trange(len(results)):
        i = 0
        is_found = False
        while(i < len(arr)):
            if (results[r] == arr[i]):
                hist[i] += 1
                is_found = True
                i = len(arr) #to escape while loop
            else:
                i = i + 1
        if(is_found == False):
            hist[58] += 1
    
    for i in range(len(hist)):
        hist[i] = hist[i] / pixel_count
    #cv2.imshow('original', img)
    #cv2.imshow('transformed', transformed_img)
    #cv2.waitKey(0)
    return hist

def create_hue_histogram(image):
    
    hue_hist = []
    print("Creating Hue histogram..")
    for i in trange(len(image)):
        for j in range(len(image[0])):
            pixel = image[i,j]
            R = pixel[2] / 255
            G = pixel[1] / 255
            B = pixel[0] / 255
            max_val = max(R, G, B)
            min_val = min(R, G, B)
            diff = max_val - min_val
            if (diff == 0):                 #evade divide-by-zero
                diff = 0.000001
            if(R == max_val):
                hue = 0.0 + (G - B) / diff
            if(G == max_val):
                hue = 2.0 + (B - R) / diff
            if(B == max_val):
                hue = 4.0 + (R - G) / diff

            hue = hue * 60
            if (hue < 0):
                hue = hue + 360
            
            hue_hist.append(hue)
            
    pixel_count = len(image) * len(image[0])
    #max_hue = np.amax(hue_hist)
    #min_hue = np.amin(hue_hist)
    for i in range(len(hue_hist)):
        #normalised_hue = hue_hist[i] / pixel_count
        #hue_hist[i] = int(round(normalised_hue))
        hue_hist[i] =  hue_hist[i] / pixel_count
    
    #hue_hist = np.array(hue_hist, dtype='float16')
    return np.array(hue_hist, dtype='float16')



test_LBP_image = cv2.imread('C:\\Users\\user\\cbir\\test\\grass6.jpg', 0)
test_hue_image = cv2.imread('C:\\Users\\user\\cbir\\test\\grass6.jpg')
#transformed_img = cv2.imread('C:\\Users\\user\\cbir\\ice\\ice10.jpg', 0)

print('Calculating histograms for test image..')
print('LBP operations:')
test_LBP_hist = create_LBP_histogram(test_LBP_image)
print('Hue operations:')
test_hue_hist = create_hue_histogram(test_hue_image)



hue_images = []
LBP_images = []
file_names = []
folder = "C:\\Users\\user\\cbir\\train"
for filename in os.listdir(folder):
    hue_img = cv2.imread(os.path.join(folder,filename))
    LBP_img = cv2.imread(os.path.join(folder,filename), 0)
    if hue_img is not None:
        hue_images.append(hue_img)
        file_names.append(filename)
    if LBP_img is not None:
        LBP_images.append(LBP_img)


#min_dist = np.finfo(np.float16).max

hue_distances = np.zeros(0, dtype ='float16')
LBP_distances = np.zeros(0, dtype ='float16')

print('Calculating histograms for train images and their distances to test image..')

for img in tqdm(hue_images):
    train_hue_hist = create_hue_histogram(img)
    hue_dist = calc_manhattan_dist(test_hue_hist, train_hue_hist)
    hue_distances = np.append(hue_distances, [hue_dist])


for img in tqdm(LBP_images):
    train_LBP_hist = create_LBP_histogram(img)
    LBP_dist = calc_manhattan_dist(test_LBP_hist, train_LBP_hist)
    LBP_distances = np.append(LBP_distances, [LBP_dist])

n = 5
min_hue_distances_inds = np.argpartition(hue_distances, n)[:n]
min_LBP_distances_inds = np.argpartition(LBP_distances, n)[:n]

print('Closest 5 images according to Hue histograms:')
for ind in min_hue_distances_inds:
    print(file_names[ind], 'Hue distance: ', hue_distances[ind])
     
    
print('Closest 5 images according to LBP histograms:')
for ind in min_LBP_distances_inds:
    print(file_names[ind], 'LBP distance: ', LBP_distances[ind])
    
  
    
"""
Results for wood1.jpg as test  

Closest 5 images according to Hue histograms:
wood6.jpg Hue distance:  4.71607488394
wood4.jpg Hue distance:  4.79811769724
wood9.jpg Hue distance:  5.78737008572
wood3.jpg Hue distance:  4.99616372585
wood2.jpg Hue distance:  4.96370947361

Closest 5 images according to LBP histograms:
foam5.jpg LBP distance:  0.0817623138428
foam9.jpg LBP distance:  0.074577331543
wood8.jpg LBP distance:  0.0299625396729
grass4.jpg LBP distance:  0.0736999511719
grass8.jpg LBP distance:  0.0491256713867
"""
    
"""
Results for grass4.jpg as test     

Closest 5 images according to Hue histograms:
ice5.jpg
foam5.jpg
foam6.jpg
ice6.jpg
foam1.jpg

Closest 5 images according to LBP histograms:
foam7.jpg
ice7.jpg
grass1.jpg
ice9.jpg
grass9.jpg
"""   

"""
Results for ice10.jpg as test

Closest 5 images according to Hue histograms:
ice2.jpg Hue distance:  2.41886019707
ice8.jpg Hue distance:  1.80328273773
ice7.jpg Hue distance:  2.16880536079
ice3.jpg Hue distance:  2.55021286011
ice6.jpg Hue distance:  4.79761981964

Closest 5 images according to LBP histograms:
foam7.jpg LBP distance:  0.0124320983887
ice6.jpg LBP distance:  0.013053894043
ice8.jpg LBP distance:  0.0108261108398
ice9.jpg LBP distance:  0.0132141113281
ice5.jpg LBP distance:  0.0133399963379
"""

"""
Results for grass6.jpg as test
Closest 5 images according to Hue histograms:
grass3.jpg Hue distance:  5.63363814354
grass10.jpg Hue distance:  5.72656965256
grass5.jpg Hue distance:  6.48250770569
grass7.jpg Hue distance:  6.69405627251
grass1.jpg Hue distance:  6.69711184502
Closest 5 images according to LBP histograms:
grass7.jpg LBP distance:  0.0336713790894
grass9.jpg LBP distance:  0.0241732597351
grass10.jpg LBP distance:  0.0313105583191
grass3.jpg LBP distance:  0.0343265533447
wood7.jpg LBP distance:  0.0344281196594
"""
    
